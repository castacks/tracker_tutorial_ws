# README #

OpenCV Tracker Tutorial for Air Lab Tutorial week 2020

### How do I get set up? ###

__Requires:__

* Git LFS `sudo apt-get install -y git-lfs`
* [Docker](https://docs.docker.com/engine/install/ubuntu/)
* ROS Melodic on Ubuntu 18.04
* RQT Image View `sudo apt install ros-melodic-rqt-image-view`
* ROS Vision Messages `sudo apt install ros-melodic-vision-msgs`

__To run & install:__

* Setup scripts assume this repo was cloned in your home folder; modify the setup script if this is not the case to have the right path
* run `scripts/setup.sh` to pull repo 
* run `scripts/run_tracker.sh` to launch docker container, which will pull the correct image

__Note:__

The first time you launch the container, you may need to re-run `catkin build` inside the container, as the script is mounting your workspace inside the container, overwriting what is inside the provided image with what you have locally


__How to run and view result:__

To run the tracker, first run a ros core on your host by typing `roscore`. In the container, type `roslaunch mbz2020_trackers tutorial.launch test:=1` to launch the tracker. 

To view the result, on your host computer, run `rosrun rqt_image_view rqt_image_view` and the image with the tracked BBox will be on topic `/tracker/bboxImage`.


### Tutorial ###

Notice how well CSRT works on video 1, which is very challenging for any tracker. Now, try changing the type of tracker used by typing `rosed mbz2020_trackers csrt_tracker.py` and change line 93 to say one of the following: 

* `cv2.TrackerCSRT_create()`
* `cv2.TrackerKCF_create()`
* `cv2.TrackerMedianFlow_create()`
* `cv2.TrackerMOSSE_create()`
* `cv2.TrackerBoosting_create()`

And any other OpenCV implements.

Notice what each tracker succeeds and fails at. Where does each tracker succeed and what does it fail at? 

If you want to try on other datasets, add them to the `data` folder and add the initial bounding box to `mbz2020_trackers/scripts/run_tests.py` to publish the initial bbox for the object you want to track.

### Known Issues ###
The node I used to publish a video as a ROS message does not loop the video properly sometimes. The workaround is to re-run the whole thing. 

### Who do I talk to? ###

* Andrew Saba (asaba@andrew.cmu.edu)
